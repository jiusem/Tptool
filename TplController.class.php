<?php
{copyright}

namespace {module}\Controller;
use Think\Controller;
class {controller}Controller extends Controller {

	/**
     * 方法功能介绍
     * @access public
     * @param mixed $db_config 
     * @return string
     */
	public function index()
	{
		//这是一个示例方法，你可以模仿这样的格式来编写其他方法
		echo 'Hello,world!';
	}
	
	public function add()
	{
		//这是一个示例方法，你可以模仿这样的格式来编写其他方法
		echo 'Hello,world!';
	}
	
	public function edit()
	{
		//这是一个示例方法，你可以模仿这样的格式来编写其他方法
		echo 'Hello,world!';
	}
	
	public function delete()
	{
		//这是一个示例方法，你可以模仿这样的格式来编写其他方法
		echo 'Hello,world!';
	}


}