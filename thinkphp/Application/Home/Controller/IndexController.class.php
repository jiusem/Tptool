<?php
// +----------------------------------------------------------------------
// |  Jiusem Inc , Tianjin , China
// +----------------------------------------------------------------------
// | Copyright (c) http://www.9sem.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: gjz<developer@9sem.net>
// +----------------------------------------------------------------------

namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {

	/**
     * 方法功能介绍
     * @access public
     * @param mixed $db_config 
     * @return string
     */
	public function index()
	{
		//这是一个示例方法，你可以模仿这样的格式来编写其他方法
		echo 'Hello,world!';
	}


}