<?php 
/*
tptool.exe源代码
*/
$c = $argv[1]; //控制器
$a = $argv[2]; //模型
$p = @$argv[3]; //参数

$copyright = file_get_contents('D:/tptool/copyright.txt');

if($c=='create'){
	if($a=='app'){
		recurse_copy('D:/tptool/thinkphp','./');
		echo 'create app successfully.';
	}else if($a=='c'){
		$arr = explode('/',$p);
		$dir = 'Application/'.$arr[0].'/Controller';
		@mkdir('Application/'.$arr[0]);
		@mkdir($dir);
		$fileName = $dir.'/'.$arr[1].'Controller.class.php';
		$fp = fopen($fileName,'w');
		$tpl = file_get_contents('D:/tptool/TplController.class.php');
		$tpl = str_replace('{module}',$arr[0],$tpl);
		$tpl = str_replace('{controller}',$arr[1],$tpl);
		$tpl = str_replace('{copyright}',$copyright,$tpl);
		fwrite($fp,$tpl);
		fclose($fp);
		echo 'create controller successfully.';
		//创建文件后立即打开
		//system('notepad++.exe '.$fileName,$res);

	}else if($a=='m'){

		$arr = explode('/',$p);
		$dir = 'Application/'.$arr[0].'/Model';
		@mkdir('Application/'.$arr[0]);
		@mkdir($dir);
		$fileName = $dir.'/'.$arr[1].'Model.class.php';
		$fp = fopen($fileName,'w');
		$tpl = file_get_contents('D:/tptool/TplModel.class.php');
		$tpl = str_replace('{module}',$arr[0],$tpl);
		$tpl = str_replace('{model}',$arr[1],$tpl);
		$tpl = str_replace('{copyright}',$copyright,$tpl);
		fwrite($fp,$tpl);
		fclose($fp);
		echo 'create model successfully.';
		//创建文件后立即打开
		//system('notepad++.exe '.$fileName,$res);

	}else if($a=='rm'){

		$arr = explode('/',$p);
		$dir = 'Application/'.$arr[0].'/Model';
		@mkdir('Application/'.$arr[0]);
		@mkdir($dir);
		$fileName = $dir.'/'.$arr[1].'Model.class.php';
		$fp = fopen($fileName,'w');
		$tpl = file_get_contents('D:/tptool/TplRelationModel.class.php');
		$tpl = str_replace('{module}',$arr[0],$tpl);
		$tpl = str_replace('{model}',$arr[1],$tpl);
		$tpl = str_replace('{copyright}',$copyright,$tpl);
		fwrite($fp,$tpl);
		fclose($fp);
		echo 'create model successfully.';
		//创建文件后立即打开
		//system('notepad++.exe '.$fileName,$res);

	}else if($a=='v'){
		$arr = explode('/',$p);
		$module = $arr[0];
		$controller = $arr[1];
		
		$dir = 'Application/'.$module.'/View/'.$controller;
		@mkdir('Application/'.$module);
		@mkdir('Application/'.$module.'/View');
		@mkdir($dir);
		
		$fileName = $dir.'/'.$arr[2].'.php';
		$fp = fopen($fileName,'w');
		$tpl = file_get_contents('D:/tptool/TplView.php');
		fwrite($fp,$tpl);
		fclose($fp);
		echo 'create view successfully.';
		//创建文件后立即打开
		//system('notepad++.exe '.$fileName,$res);
	}
}
/**
关闭open方法
else if($c=='open'){
	if($a=='c'){
		$arr = explode('/',$p);
		$dir = 'Application/'.$arr[0].'/Controller';
		$fileName = $dir.'/'.$arr[1].'Controller.class.php';
		if(file_exists($fileName)){
		system('notepad++.exe '.$fileName,$res);
		echo 'open successfully';
		}else{
			echo 'file not exists.';
		}
	}else if($a=='m'){
		$arr = explode('/',$p);
		$dir = 'Application/'.$arr[0].'/Model';
		$fileName = $dir.'/'.$arr[1].'Model.class.php';
		if(file_exists($fileName)){
		system('notepad++.exe '.$fileName,$res);
		echo 'open successfully';
		}else{
			echo 'file not exists.';
		}
	}else if($a=='v'){
		$arr = explode('/',$p);
		$dir = 'Application/'.$arr[0].'/View/'.$arr[1];
		$fileName = $dir.'/'.$arr[2].'.php';
		if(file_exists($fileName)){
		system('notepad++.exe '.$fileName,$res);
		echo 'open successfully';
		}else{
			echo 'file not exists.';
		}
	}
}
**/


function recurse_copy($src,$dst) {  // 原目录,复制到的目录

    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                recurse_copy($src . '/' . $file,$dst . '/' . $file);
            }
            else {
                copy($src . '/' . $file,$dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}
?>